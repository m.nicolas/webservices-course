const { Router } = require('express')
const router = Router()

const {
    getBooks,
    getBookByID,
    createBook,
    updateBook,
    deleteBook
} = require('../controllers/index.controller.js')

router.get('/books', getBooks)
router.get('/book/:id', getBookByID)

router.post('/book', createBook)

router.put('/book/:id', updateBook)

router.delete('/book/:id', deleteBook)

module.exports = router