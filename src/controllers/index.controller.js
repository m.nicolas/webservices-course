const { Pool } = require('pg')

const pool = new Pool({
    host: 'localhost',
    user: 'postgres',
    password: '', // Passwd by the postgres init
    database: 'webservicesapi',
    port: '5432'
})

const getBooks = async (req, res) => {
    const response = await pool.query('SELECT * FROM books')
    console.log(response.rows)
    res.status(200).json(response.rows)
}

const getBookByID = async (req, res) => {
    const id = req.params.id
    const response = await pool.query('SELECT * FROM books WHERE id = $1', [id])
    res.status(200).json(response.rows)
}

const createBook = async (req, res) => {
    const { name, autor, date } = req.body

    const response = await pool.query('INSERT INTO books (name, autor, date) VALUES ($1, $2, $3)', [name, autor, date])
    console.log(response)
    res.status(200).json({
        message: 'Book added successfully !', 
        body: {
            book: name, autor, date
        }
    })
}

const updateBook = async (req, res) => {
    const id = req.params.id
    const { name, autor, date } = req.body

    console.log(name, autor, date, id)

    const response = await pool.query('UPDATE books SET name = $1, autor = $2, date = $3 WHERE id = $4', [
        name,
        autor,
        date,
        id
    ])
    console.log(response)
    res.status(200).json('User update successfully !')
}

const deleteBook = async (req, res) => {
    const id = req.params.id
    const response = await pool.query('DELETE FROM books WHERE id = $1', [id])
    console.log(response)
    res.status(200).json(`Book with id: ${id} deleted successfully !`)

}

module.exports = {
    getBooks: getBooks,
    getBookByID: getBookByID,
    createBook: createBook,
    updateBook: updateBook,
    deleteBook: deleteBook
}