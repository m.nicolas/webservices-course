# Introduction

This project was for webservices course

# Start the project

I use PostgreSQL on this project.
Init the db with sql script in `database/data.sql`

Install dependencies with :
```
npm i
```

Run it with: 
```
npm run dev
```