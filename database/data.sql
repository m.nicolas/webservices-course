CREATE DATABASE webservicesapi;

CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    name VARCHAR(40),
    autor VARCHAR(40),
    date DATE NOT NULL DEFAULT CURRENT_DATE
);

INSERT INTO users (name, email) VALUES
    ("La Communauté de l'Anneau", "J.R.R Tolkien", "1954-11-11")
